﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace WindowsFormsApp1
{
    public partial class app : Form
    {
        public app()
        {
            InitializeComponent();
        }

        private void app_Load(object sender, EventArgs e)
        {
            panel1.Show(); //로그인 화면
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool C = ConfigurationManager.AppSettings["id"] == textBox1.Text; //오류?
            if (C)
            {
                panel1.Hide();
                panel2.Show();
            }
        }
    }
}
