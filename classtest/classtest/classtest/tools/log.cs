﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using classtest;
using classtest.tools;
using System.IO;

namespace classtest.tools
{
    class log //로그 기능
    {
        //경로
        private string _path;

     

        public log() //경로 정의
        {

            _path = System.IO.Path.Combine(classtest.tools.Class1.path_,"data");

        }

        private void _setpath() //경로 정의
        {
            if (!System.IO.Directory.Exists(_path))
            {
                System.IO.Directory.CreateDirectory(_path);
            }
        }

        public void write(string data) //기록
        {
            try
            {
                using (StreamWriter write = new StreamWriter(_path, true))
                {
                    write.WriteLine(data);
                }
            }

            catch(Exception ex) { }

        }

    }
}
